import java.util.Scanner;

public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	public String toString(){
		return 	"--------------------------------- \n" + 
				"Center card: " + centerCard + "\n" +
				"Player card: " + playerCard + "\n" +
				"--------------------------------- \n";
	}
	public void dealCards(){
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		drawPile.shuffle();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return drawPile.getLenOfDeck();
	}
	public int calculatePoints(){
		if(centerCard.getValue() == playerCard.getValue()){
			return 4;
		}else if(centerCard.getSuit().equals(playerCard.getSuit())){
			return 2;
		}else {
			return -1;
		}
	}
}