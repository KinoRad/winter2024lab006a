public class Card{
	
	private String suit;
	private int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return suit;
	}
	
	public void setSuit(String newSuit){
		this.suit = newSuit;
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int newValue){
		this.value = newValue;
	}
	
	public String toString(){
		String tempValue = "";
		if(value == 1){
			tempValue = "Ace";
		}else if(value == 11){
			tempValue = "Jack";
		}else if(value == 12){
			tempValue = "Queen";
		}else if(value == 13){
			tempValue = "King";
		} else {
			tempValue += value;
		}
		return tempValue + " of " + suit;
	}
}